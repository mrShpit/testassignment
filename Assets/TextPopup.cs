﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class TextPopup : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI _textPopup;

    public void SetText(string text, Color color)
    {
        _textPopup.text = text;
        _textPopup.color = color;
    }

    public void DestroyTextPopup()
    {
        Destroy(this.gameObject);
    }
}
