﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOverScreenController : MonoBehaviour
{
    [SerializeField] private GameObject _gameOverScreen;
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private Button _reTryBtn;
    [SerializeField] private Button _exitBtn;

    private void Awake()
    {
        _reTryBtn.onClick.AddListener(ReTryBtnPressed);
        _exitBtn.onClick.AddListener(ExitBtnPressed);
    }

    public void SetScreenActive(bool active)
    {
        _gameOverScreen.SetActive(active);
    }

    public void SetScore(int score)
    {
        _scoreText.text = string.Format("Your score: <color=red>{0}</color>", score);
    }

    private void ReTryBtnPressed()
    {
        _gameOverScreen.SetActive(false);
        GameManager.Instance.StartGame();
    }

    private void ExitBtnPressed()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(Constants.MainMenuSceneName);
        LoadingScreenController.Instance.InitiateLoadingScreen(async);
    }
}