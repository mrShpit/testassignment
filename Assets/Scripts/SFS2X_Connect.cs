﻿using Sfs2X;
using Sfs2X.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SFS2X_Connect : MonoBehaviour
{
    public string ServerIP = "127.0.0.1";
    public int ServerPort = 9933;

    SmartFox sfs;

    private void Start()
    {
        sfs = new SmartFox();
        sfs.ThreadSafeMode = true;

        sfs.AddEventListener(SFSEvent.CONNECTION, OnConnection);
        sfs.Connect(ServerIP, ServerPort);
    }

    private void Update()
    {
        sfs.ProcessEvents();
    }

    void OnConnection(BaseEvent e)
    {
        MainMenuController.Instance.SetConnectionStatus((bool)e.Params["success"]);
    }

    void OnApplicationQuit()
    {
        if (sfs.IsConnected)
        {
            sfs.Disconnect();
        }
    }
}
