﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GhostController : MonoBehaviour
{
    [SerializeField] private Animator _animator;

    [Header("Sprite Renderers")]
    [SerializeField] private SpriteRenderer _bodySR;
    [SerializeField] private SpriteRenderer _eyesSR;
    [SerializeField] private SpriteRenderer _mouthSR;

    [Header("Sprites")]
    [SerializeField] private Sprite _normalEyes;
    [SerializeField] private Sprite _damageEyes;
    [SerializeField] private Sprite _normalMouth;
    [SerializeField] private Sprite _damageMouth;

    [Header("Parameters")]
    [SerializeField] private int _health;
    [SerializeField] private float _damageEffectDuration;
    [SerializeField] private float _shakeAmount;
    [SerializeField] private float _routeIntervalDistance;
    [SerializeField] private float _moveSpeed;

    private float _damageEffectTimer;
    private Vector2[] _routePoints;
    private float _lifeTime;
    private bool isVulnerable = true;

    private GhostColor _ghostTint;
    public GhostColor GhostTint
    {
        get => _ghostTint;
        set => _ghostTint = value;
    }

    public UnityAction GhostDestroyed;
    
    private IEnumerator DamageEffect()
    {
        _damageEffectTimer = _damageEffectDuration;
        _eyesSR.sprite = _damageEyes;
        _mouthSR.sprite = _damageMouth;

        while (_damageEffectTimer > 0)
        {
            Vector2 newPos = Random.insideUnitSphere * (Time.deltaTime * _shakeAmount * _damageEffectTimer);
            _bodySR.transform.localPosition = newPos;
            _damageEffectTimer -= Time.deltaTime;
            yield return null;
        }

        _bodySR.transform.localPosition = Vector2.zero;

        _eyesSR.sprite = _normalEyes;
        _mouthSR.sprite = _normalMouth;
    }
    
    private IEnumerator PerformRandomBezierMoving(float clampDistance)
    {
        Vector2 startBezierPoint = transform.position;

        while (true)
        {
            float moveProgress = 0f;

            Vector2 endBezierPoint = GameManager.Instance.GetRandomPointInCameraClampedArea(clampDistance);
            Vector2 adjustmentPoint1 = GameManager.Instance.GetRandomPointInCameraClampedArea(clampDistance);
            Vector2 adjustmentPoint2 = GameManager.Instance.GetRandomPointInCameraClampedArea(clampDistance);

            Vector2[] currentBezierCurve = new Vector2[] { startBezierPoint, adjustmentPoint1, adjustmentPoint2, endBezierPoint };

            while (moveProgress < 1)
            {
                if ((_lifeTime -= Time.deltaTime) < 0)
                {
                    StartHideAnimation();
                }
                
                moveProgress += Time.deltaTime * _moveSpeed;
                Vector2 nextPoint = CalculateBezierPoint(currentBezierCurve, moveProgress);
                transform.localScale = nextPoint.x > transform.position.x ? new Vector3(-1, 1, 1) : new Vector3(1, 1, 1);
                transform.position = nextPoint;

                yield return null;
            }

            startBezierPoint = endBezierPoint;
        }
    }

    private Vector2 CalculateBezierPoint(Vector2[] controlPoints, float step)
    {
        return Mathf.Pow(1 - step, 3) * controlPoints[0] +
            3 * Mathf.Pow(1 - step, 2) * step * controlPoints[1] +
            3 * (1 - step) * Mathf.Pow(step, 2) * controlPoints[2] +
            Mathf.Pow(step, 3) * controlPoints[3];
    }

    private void StartHideAnimation()
    {
        isVulnerable = false;
        _animator.SetTrigger("Hide");
    }

    private void KillGhost()
    {
        isVulnerable = false;
        _animator.SetTrigger("Dead");
    }

    public void SetupGhost(float lifeTime, float areaClamp, ColorTint colorParams)
    {
        _lifeTime = lifeTime;
        StartCoroutine(PerformRandomBezierMoving(areaClamp));
        _ghostTint = colorParams.TintColorName;
        _bodySR.color = colorParams.TintColor;
    }

    public void SetSpriteLayer(ref int layer)
    {
        _bodySR.sortingOrder = layer++;
        _eyesSR.sortingOrder = layer++;
        _mouthSR.sortingOrder = layer++;
    }

    public bool TakeDamage()
    {
        if (!isVulnerable)
        {
            return false;
        }
        else
        {
            _health--;
        }

        if (_health <= 0)
        {
            KillGhost();
            return true;
        }

        if (_damageEffectTimer > 0)
        {
            _damageEffectTimer = _damageEffectDuration;
        }
        else
        {
            StartCoroutine(DamageEffect());
        }

        return true;
    }

    public void DestroyGhost()
    {
        GhostDestroyed.Invoke();
        Destroy(this.gameObject);
    }
}
