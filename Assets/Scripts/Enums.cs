﻿public enum SceneName
{
    GameScene,
    MainMenu
}

public enum GhostColor
{
    Red,
    Violet,
    Orange,
    Green,
    White
}