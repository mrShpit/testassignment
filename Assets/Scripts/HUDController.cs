﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class HUDController : MonoBehaviour
{
    public static HUDController Instance;

    [SerializeField] private GameObject __hudPanel;
    [SerializeField] private GameObject __pausePanel;
    [SerializeField] private Button _pauseBtn;
    [SerializeField] private Button _exitBtn;
    [SerializeField] private Button _unpauseBtn;
    [SerializeField] private TextMeshProUGUI _scoreText;
    [SerializeField] private TextMeshProUGUI _timeText;
    [SerializeField] private TextMeshProUGUI _objectiveText;
    [SerializeField] private Image _objectiveTimerClock;
    [SerializeField] private Animator _objectiveTextAnimator;

    private bool _objectiveTextIsVisible;
    private string _nextObjectiveText;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            _objectiveTextIsVisible = false;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        _pauseBtn.onClick.AddListener(GameManager.Instance.PauseClicked);
        _unpauseBtn.onClick.AddListener(GameManager.Instance.UnPauseClicked);
        _exitBtn.onClick.AddListener(ExitGame);
    }

    private void ExitGame()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(Constants.MainMenuSceneName);
        LoadingScreenController.Instance.InitiateLoadingScreen(async);
    }

    public void SetHUDActive(bool active)
    {
        __hudPanel.SetActive(active);
    }

    public void SetPauseHUDActive(bool active)
    {
        _pauseBtn.gameObject.SetActive(!active);
        __pausePanel.gameObject.SetActive(active);
    }

    public void SetScoreText(int score)
    {
        _scoreText.text = string.Format("Score: <color=orange>{0}</color>", score);
    }

    public void SetTimeText(double time)
    {
        _timeText.text = string.Format("Time left: <color=red>{0}</color>", System.Math.Round(time, 1, System.MidpointRounding.AwayFromZero));
    }

    public void SetNewObjective(bool isObjectiveStraight, List<GhostColor> colors)
    {
        string objectiveColors = String.Join(" and ", colors);

        _nextObjectiveText = string.Format(
            isObjectiveStraight ? Constants.ObjectiveText_Only : Constants.ObjectiveText_AllExcept, objectiveColors);

        if (_objectiveTextIsVisible)
        {
            _objectiveTextAnimator.SetTrigger("Close");
            _objectiveTextIsVisible = false;
        }
        else
        {
            UpdateAndShowObjectiveText();
        }
    }

    public void UpdateAndShowObjectiveText()
    {
        _objectiveTextIsVisible = true;
        _objectiveText.text = _nextObjectiveText;
        _objectiveTextAnimator.SetTrigger("Open");
    }

    public void SetObjectiveTimerProgress(float fillAmount)
    {
        _objectiveTimerClock.fillAmount = fillAmount;
    }
}
