﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class MainMenuController : MonoBehaviour
{
    public static MainMenuController Instance;

    [SerializeField] private Button _gameStartBtn;
    [SerializeField] private Button _gameExitBtn;
    [SerializeField] private TextMeshProUGUI _connectionTest;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    void Start()
    {
        _gameStartBtn.onClick.AddListener(LoadGameScene);
        _gameExitBtn.onClick.AddListener(ExitApplication);
    }

    private void LoadGameScene()
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(Constants.GameSceneMame);
        LoadingScreenController.Instance.InitiateLoadingScreen(async);
    }

    private void ExitApplication()
    {
        Application.Quit();
    }

    public void SetConnectionStatus(bool connection)
    {
        _connectionTest.text = connection ? Constants.ConnectedToServerText : Constants.ConnectionToServerFailedText;
    }
}
