﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LoadingScreenController : MonoBehaviour
{
    public static LoadingScreenController Instance;
    
    [SerializeField] private GameObject _loadingPanel;
    [SerializeField] private Slider _loadingSlider;
    [SerializeField] private TextMeshProUGUI _loadingText;
    
    public void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(this);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    public void InitiateLoadingScreen(AsyncOperation sceneLoading)
    {
        _loadingPanel.SetActive(true);

        StartCoroutine(ShowLoadingProgress(sceneLoading));
    }

    private IEnumerator ShowLoadingProgress(AsyncOperation sceneLoading)
    {
        while (!sceneLoading.isDone)
        {
            float progress = Mathf.Clamp01(sceneLoading.progress / 0.9f);
            _loadingSlider.value = progress;
            _loadingText.text = string.Format("{0}%", progress * 100);
            yield return null;
        }

        _loadingPanel.SetActive(false);
    }
}
