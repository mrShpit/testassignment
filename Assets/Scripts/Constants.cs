﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Constants
{
    public const string MainMenuSceneName = "MainMenu";
    public const string GameSceneMame = "Game";

    public const string ObjectiveText_Only = "Hit {0} ghosts only";
    public const string ObjectiveText_AllExcept = "Hit all ghosts except {0}";

    public const string ConnectedToServerText = "Connected to the local server";
    public const string ConnectionToServerFailedText = "Connection to local server failed. Game will proceed in offline mode";
}
