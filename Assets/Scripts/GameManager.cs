﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager Instance;
    
    [SerializeField] private GameOverScreenController _gameOverScreen;

    [Header("Prefabs")]
    [SerializeField] private GameObject _ghostPrefab;
    [SerializeField] private GameObject _bonusTextPrefab;

    [Header("Ghost settings")]
    [SerializeField] private float _moveAreaClamp;
    [SerializeField] private int _ghostMinLifeTime;
    [SerializeField] private int _ghostMaxLifeTime;
    [SerializeField] private float _spawnRate;
    [SerializeField] private float _spawnChance;

    [Header("Game settings")]
    [SerializeField] private float _roundTime;
    [SerializeField] private float _objectiveChangeFrequency;
    [SerializeField] private int _pointsForHit;
    [SerializeField] private List<ColorTint> _colorTints;

    private bool _canHit;
    private int _currentLayer;
    private float _timer;
    private int _score;
    private bool _currentObjectiveIsToHitOnly;
    private GhostColor[] _currentObjectiveColors;

    public float Timer
    {
        get => _timer;
        set
        {
            _timer = value;
            HUDController.Instance.SetTimeText(_timer);
        }
    }

    public int Score
    {
        get => _score;
        set
        {
            _score = value;

            if (_score < 0)
            {
                _score = 0;
            }

            HUDController.Instance.SetScoreText(_score);
        }
    }

    private Vector2 _cameraMinPoint;
    private Vector2 _cameraMaxPoint;

    private List<GhostController> ghostsOnScreen;
    
    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;

            float height = 2f * Camera.main.orthographicSize;
            float width = height * Camera.main.aspect;

            _cameraMinPoint = new Vector2(Camera.main.transform.position.x - width / 2f, Camera.main.transform.position.y - height / 2f);
            _cameraMaxPoint = new Vector2(Camera.main.transform.position.x + width / 2f, Camera.main.transform.position.y + height / 2f);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }

    private void Start()
    {
        StartGame();
    }

    private void Update()
    {
#if UNITY_ANDROID
        
        if (Input.touchCount != 1)
        {
            return;
        }

        Touch touch = Input.GetTouch(0);
        if (touch.phase == TouchPhase.Ended) _canHit = true;
        if (touch.phase == TouchPhase.Began) _canHit = false;
        
        if (_canHit)
        {
            RaycastHit2D rayHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.GetTouch(0).position), Vector2.zero);

            if (rayHit.collider != null && rayHit.collider.gameObject.CompareTag("Ghost"))
            {
                HitGhost(rayHit.collider.gameObject.GetComponent<GhostController>());
            }
        }

#endif

#if UNITY_EDITOR
        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit2D rayHit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);

            if (rayHit.collider != null && rayHit.collider.gameObject.CompareTag("Ghost"))
            {
                HitGhost(rayHit.collider.gameObject.GetComponent<GhostController>());
            }
        }
#endif
    }

    private void HitGhost(GhostController ghost)
    {
        if (!ghost.TakeDamage())
        {
            return;
        }

        if ( (_currentObjectiveIsToHitOnly && _currentObjectiveColors.Contains(ghost.GhostTint)) ||
            (!_currentObjectiveIsToHitOnly && !_currentObjectiveColors.Contains(ghost.GhostTint)) )
        {
            Score += _pointsForHit;
            TextPopup textPopup = Instantiate(_bonusTextPrefab, ghost.transform.position, Quaternion.identity).GetComponent<TextPopup>();
            textPopup.SetText(string.Format("+{0}", _pointsForHit), Color.green);
        }
        else
        {
            Score -= _pointsForHit;
            TextPopup textPopup = Instantiate(_bonusTextPrefab, ghost.transform.position, Quaternion.identity).GetComponent<TextPopup>();
            textPopup.SetText(string.Format("-{0}", _pointsForHit), Color.red);
        }
    }

    private IEnumerator GameProcess()
    {
        _currentLayer = 0;
        ghostsOnScreen = new List<GhostController>();

        float nextSpawnTimerValue = _roundTime;
        float objectiveUpdateTime = 0f;
        GenerateNewObjective();

        while (Timer > 0)
        {
            Timer -= Time.deltaTime;
            objectiveUpdateTime += Time.deltaTime;

            HUDController.Instance.SetObjectiveTimerProgress(objectiveUpdateTime / _objectiveChangeFrequency);

            if (Timer <= nextSpawnTimerValue)
            {
                nextSpawnTimerValue -= _spawnRate;
                if (new System.Random().NextDouble() < _spawnChance) SpawnGhost();
            }

            if (objectiveUpdateTime > _objectiveChangeFrequency)
            {
                objectiveUpdateTime = 0f;
                GenerateNewObjective();
            }


            yield return null;
        }

        EndGame();
    }

    private void GenerateNewObjective()
    {
        _currentObjectiveIsToHitOnly = UnityEngine.Random.Range(0, 3) > 0;
        int objectiveColorsAmount = UnityEngine.Random.Range(1, 3);
        List<GhostColor> objectiveColorsList = new List<GhostColor>();
        for (int i = 0; i < objectiveColorsAmount; i++)
        {
            GhostColor color = _colorTints[UnityEngine.Random.Range(0, _colorTints.Count - 1)].TintColorName;
            objectiveColorsList.Add(color);
        }

        if (objectiveColorsAmount > 1 && (objectiveColorsList[0] == objectiveColorsList[1]))
        {
            objectiveColorsList.RemoveAt(1);
        }

        _currentObjectiveColors = objectiveColorsList.ToArray();
        HUDController.Instance.SetNewObjective(_currentObjectiveIsToHitOnly, objectiveColorsList);
    }

    private void EndGame()
    {
        foreach(GhostController ghost in ghostsOnScreen)
        {
            Destroy(ghost.gameObject);
        }

        Time.timeScale = 0f;
        HUDController.Instance.SetHUDActive(false);
        _gameOverScreen.SetScreenActive(true);
        _gameOverScreen.SetScore(_score);
    }

    private void SpawnGhost()
    {
        Vector3 initialPosition = GetRandomPointInCameraClampedArea(_moveAreaClamp);
        GhostController ghost = Instantiate(_ghostPrefab, initialPosition, Quaternion.identity).GetComponent<GhostController>();
        ghostsOnScreen.Add(ghost);

        ghost.SetSpriteLayer(ref _currentLayer);
        ghost.SetupGhost(
            UnityEngine.Random.Range(_ghostMinLifeTime, _ghostMaxLifeTime), 
            _moveAreaClamp,
            _colorTints[UnityEngine.Random.Range(0, _colorTints.Count)]);

        ghost.GhostDestroyed += delegate 
        {
            ghostsOnScreen.Remove(ghost);
        };
    }

    public void StartGame()
    {
        Timer = _roundTime;
        Score = 0;
        Time.timeScale = 1f;
        HUDController.Instance.SetHUDActive(true);

        StartCoroutine(GameProcess());
    }

    public Vector2 GetRandomPointInCameraClampedArea(float clampDistance)
    {
        return new Vector2(
            UnityEngine.Random.Range(_cameraMinPoint.x + clampDistance, _cameraMaxPoint.x - clampDistance),
            UnityEngine.Random.Range(_cameraMinPoint.y + clampDistance, _cameraMaxPoint.y - clampDistance));
    }

    public void PauseClicked()
    {
        Time.timeScale = 0f;
        HUDController.Instance.SetPauseHUDActive(true);
    }

    public void UnPauseClicked()
    {
        Time.timeScale = 1f;
        HUDController.Instance.SetPauseHUDActive(false);
    }
}

[Serializable]
public struct ColorTint
{
    public Color TintColor;
    public GhostColor TintColorName;
}